module PFM
  # @author BlasterMillennia
  class PokemonBattler < Pokemon
    remove_const :COPIED_PROPERTIES
    COPIED_PROPERTIES = %i[
      @id @form @given_name @code @ability @nature @ip
      @iv_hp @iv_atk @iv_dfe @iv_spd @iv_ats @iv_dfs
      @ev_hp @ev_atk @ev_dfe @ev_spd @ev_ats @ev_dfs
      @trainer_id @trainer_name @step_remaining @loyalty
      @exp @hp @status @status_count @item_holding
      @captured_with @captured_in @captured_at @captured_level
      @gender @skill_learnt @ribbons @character
      @exp_rate @hp_rate @egg_at @egg_in
    ]
  end
end
