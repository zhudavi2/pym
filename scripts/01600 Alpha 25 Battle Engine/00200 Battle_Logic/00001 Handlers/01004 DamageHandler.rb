# @author BlasterMillennia
module Battle
  class Logic
    # Clear to-be-distributed exp for just-fainted Pokémon
    DamageHandler.register_post_damage_death_hook('PSDK post damage death: Clear exp') do |handler, _, target, _, _|
      handler.logic.exp_distributions.delete(target)
    end
  end
end
