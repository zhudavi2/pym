# @author BlasterMillennia
module Battle
  class Logic
    BattleEndHandler.register('PSDK distribute exp after battle') do |handler|
      handler.scene.visual.show_exp_distribution(handler.logic.exp_distributions) if handler.logic.exp_distributions.any?
    end
  end
end
