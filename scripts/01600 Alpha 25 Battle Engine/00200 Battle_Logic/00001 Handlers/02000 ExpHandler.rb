# @author BlasterMillennia
module Battle
  class Logic
    class ExpHandler
      def distribute_exp_for(enemy)
        DZDebug.log(DZDebug::LogLevel::ENTRY, self, "enemy = #{enemy}")
        exp_hash = {}
        if !logic.battle_info.disallow_exp?
          expable = expable_pokemon(enemy)

          distribute_ev_to(evable_pokemon(enemy), enemy)
          distribute_ip_to(ipable_pokemon(enemy), enemy)
          exp_data = global_multi_exp_factor? ? distribute_global_exp_for(enemy, expable) : distribute_separate_exp_for(enemy, expable)
          enemy.exp_distributed = true
          exp_hash = exp_data.to_h
        end
        DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning #{exp_hash}")
        return exp_hash
      end

      def expable_pokemon(enemy)
        DZDebug.log(DZDebug::LogLevel::ENTRY, self, "enemy = #{enemy}")
        expable = logic.trainer_battlers.reject do |receiver|
          next receiver.max_level == receiver.level || receiver.dead? || !(receiver.encountered?(enemy) || receiver.item_db_symbol == :exp_share)
        end
        DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning #{expable}")
        return expable
      end

      def distribute_separate_exp_for(enemy, expable)
        DZDebug.log(DZDebug::LogLevel::ENTRY, self, "enemy = #{enemy}, expable = #{expable}")
        base_exp = exp_base(enemy)
        expable.each { |battler| raise "Fainted battler gaining exp" if battler.dead? }
        multi_exp_count = expable.count { |battler| battler.item_db_symbol == :exp_share }
        multi_exp_factor = exp_multi_exp_factor(multi_exp_count)
        fought_exp_factor = exp_fought_factor(multi_exp_count, expable.count { |receiver| receiver.encountered?(enemy) })
        DZDebug.log(DZDebug::LogLevel::INFO1, self, "Base exp #{base_exp}, Exp Share count #{multi_exp_count}, encountering receivers' factor #{fought_exp_factor}")
        exp_data = expable.map do |receiver|
          level_multiplier = level_multiplier(enemy.level, receiver.level)
          DZDebug.log(DZDebug::LogLevel::INFO1, self, "#{receiver} level multiplier #{level_multiplier}")
          exp = (base_exp * level_multiplier * exp_multipliers(receiver)).floor
          if !receiver.encountered?(enemy)
            raise "Exp receiver gaining exp without battling or Exp Share" if receiver.item_db_symbol != :exp_share
            next [receiver, (exp / multi_exp_factor).to_i]
          else
            receiver.delete_battler_to_encounter_list(enemy)
            next [receiver, (exp / fought_exp_factor).to_i + (receiver.item_db_symbol == :exp_share ? exp / multi_exp_factor : 0).to_i]
          end
        end
        DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning #{exp_data}")
        return exp_data
      end

      # Get the list of Pokémon that should receive IP
      # @param enemy [PFM::PokemonBattler]
      # @return [Array<PFM::PokemonBattler>]
      def ipable_pokemon(enemy)
        DZDebug.log(DZDebug::LogLevel::ENTRY, self, "enemy = #{enemy}")
        ipable = logic.trainer_battlers.reject do |receiver|
          next receiver.dead? || !receiver.encountered?(enemy) || (receiver.item_db_symbol != :macho_brace)
        end
        DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning #{ipable}")
        return ipable
      end

      # Distribute IP to ipables depending on the enemy that was taken down
      # @param ipable [Array<PFM::PokemonBattler>]
      # @param enemy [PFM::PokemonBattler]
      def distribute_ip_to(ipable, enemy)
        DZDebug.log(DZDebug::LogLevel::ENTRY, self, "ipable = #{ipable}, enemy = #{enemy}")
        DZDebug.log(DZDebug::LogLevel::INFO1, self, "Distributing IP to [#{ipable.join(', ')}]")
        ipable.map(&:original).each do |receiver| # <= Pokemon will receive IP only at end of battle
          receiver.add_ip(enemy.ip_battle_list)
        end
        DZDebug.log(DZDebug::LogLevel::EXIT, self, "")
      end
    end
  end
end
