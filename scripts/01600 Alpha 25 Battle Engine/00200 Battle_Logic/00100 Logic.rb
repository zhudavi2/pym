# @author BlasterMillennia
module Battle
  class Logic
    # Exp to be distributed at battle-end
    # @return [Hash{ PFM::PokemonBattler => Integer }]
    attr_reader :exp_distributions

    alias old_initialize initialize
    def initialize(scene)
      old_initialize(scene)
      @exp_distributions = {}
    end
  end
end
