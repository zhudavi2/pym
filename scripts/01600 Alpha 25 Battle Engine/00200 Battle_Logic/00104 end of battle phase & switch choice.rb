# @author BlasterMillennia
module Battle
  class Logic
    def battle_phase_exp
      exp_distributions = exp_handler.distribute_exp_grouped(dead_enemy_battler_during_this_turn)
      exp_distributions.each do |receiver, exp|
        @exp_distributions[receiver] = 0 if !@exp_distributions.include?(receiver)
        @exp_distributions[receiver] += exp
      end
    end
  end
end
