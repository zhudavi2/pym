# @author BlasterMillennia
module UI
  class Summary_Stat < SpriteStack
    def init_stats
      @stat_name_texts = []
      texts = text_file_get(27)
      with_surface(114, 19, 70) do
        # --- Static part ---
        @nature_text = add_line(0, '') # Nature
        add_line(1, texts[15]) # HP
        @stat_name_texts << add_line(2, texts[18]) # Attack
        @stat_name_texts << add_line(3, texts[20]) # Defense
        @stat_name_texts << add_line(4, texts[22]) # Attack Spe
        @stat_name_texts << add_line(5, texts[24]) # Defense Spe
        @stat_name_texts << add_line(6, texts[26]) # Speed
        # --- Data part ---
        add_line(1, :hp_text, 2, type: SymText, color: 1)
        add_line(2, :atk_basis, 2, type: SymText, color: 1)
        add_line(3, :dfe_basis, 2, type: SymText, color: 1)
        add_line(4, :ats_basis, 2, type: SymText, color: 1)
        add_line(5, :dfs_basis, 2, type: SymText, color: 1)
        add_line(6, :spd_basis, 2, type: SymText, color: 1)
      end
      init_ev_iv
    end

    # Init the ev/ip/iv texts
    def init_ev_iv
      offset = 77
      # --- EV part ---
      if SHOW_EV
        with_surface(114 + offset, 19, 95) do
          add_line(1, :ev_hp_text, type: SymText)
          add_line(2, :ev_atk_text, type: SymText)
          add_line(3, :ev_dfe_text, type: SymText)
          add_line(4, :ev_ats_text, type: SymText)
          add_line(5, :ev_dfs_text, type: SymText)
          add_line(6, :ev_spd_text, type: SymText)
        end
        offset += 44
      end
      # --- IP/IV part ---
      if SHOW_IV
        with_surface(114 + offset, 19, 95) do
          add_line(1, :ip_hp_text, type: SymText)
          add_line(2, :ip_atk_text, type: SymText)
          add_line(3, :ip_dfe_text, type: SymText)
          add_line(4, :ip_ats_text, type: SymText)
          add_line(5, :ip_dfs_text, type: SymText)
          add_line(6, :ip_spd_text, type: SymText)
        end
      end
    end
  end
end
