module PFM
  # @author BlasterMillennia
  class Pokemon
    # Individual Points
    attr_writer :ip

    # @return [Array<Integer>] IP list: [hp, atk, dfe, spd, ats, dfs]
    def ip
      if @ip == nil
        DZDebug.log(DZDebug::LogLevel::WARNING, self, "No IP data for #{self}")
        ip_data_initialize
      end
      @ip
    end
  end
end
