module PFM
  # @author BlasterMillennia
  class Pokemon
    def calculate_ip_from_iv(iv)
      (iv == 0) ? 0 : (((iv + 1) ** 2) * 32)
    end

    alias old_iv_data_initialize iv_data_initialize
    def iv_data_initialize(opts)
      DZDebug.log(DZDebug::LogLevel::ENTRY, self.class.name, "opts = #{opts}")
      old_iv_data_initialize(opts)
      ip_data_initialize
      DZDebug.log(DZDebug::LogLevel::EXIT, self, "")
    end

    def ip_data_initialize
      DZDebug.log(DZDebug::LogLevel::ENTRY, self, "")

      @ip = Array.new(6, 0)
      stats = Configs.stats

      @ip[stats.hp_index] = calculate_ip_from_iv(@iv_hp)
      DZDebug.log(DZDebug::LogLevel::INFO1, self, "HP IV #{@iv_hp}, HP IP #{@ip[stats.hp_index]}")

      @ip[stats.atk_index] = calculate_ip_from_iv(@iv_atk)
      DZDebug.log(DZDebug::LogLevel::INFO1, self, "Attack IV #{@iv_atk}, Attack IP #{@ip[stats.atk_index]}")

      @ip[stats.dfe_index] = calculate_ip_from_iv(@iv_dfe)
      DZDebug.log(DZDebug::LogLevel::INFO1, self, "Defense IV #{@iv_dfe}, Defense IP #{@ip[stats.dfe_index]}")

      @ip[stats.spd_index] = calculate_ip_from_iv(@iv_spd)
      DZDebug.log(DZDebug::LogLevel::INFO1, self, "Speed IV #{@iv_spd}, Speed IP #{@ip[stats.spd_index]}")

      @ip[stats.ats_index] = calculate_ip_from_iv(@iv_ats)
      DZDebug.log(DZDebug::LogLevel::INFO1, self, "Special Attack IV #{@iv_ats}, Special Attack IP #{@ip[stats.ats_index]}")

      @ip[stats.dfs_index] = calculate_ip_from_iv(@iv_dfs)
      DZDebug.log(DZDebug::LogLevel::INFO1, self, "Special Defense IV #{@iv_dfs}, Special Defense IP #{@ip[stats.dfs_index]}")

      DZDebug.log(DZDebug::LogLevel::INFO1, self, "IPs: #{@ip}")
      
      DZDebug.log(DZDebug::LogLevel::EXIT, self, "")
    end
  end
end
