module GamePlay
  # @author BlasterMillennia
  class Save
    class << self
      # Save a game
      # @param filename [String, nil] name of the save file (nil = auto name the save file)
      # @param no_file [Boolean] tell if the save should not be saved to file and just be returned
      def save(filename = nil, no_file = false)
        DZDebug.log(DZDebug::LogLevel::ENTRY, self, "filename = #{filename}, no_file = #{no_file}}")
        if !$game_temp
          DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning NONE")
          return 'NONE'
        end

        clear_states
        update_save_info
        # Call the hooks that make the save data safer and lighter
        BEFORE_SAVE_HOOKS.each_value(&:call)
        # Build the save data
        save_data = Configs.save_config.save_header.dup.force_encoding(Encoding::ASCII_8BIT)
        save_data << encrypt(Marshal.dump($pokemon_party))
        # Save the game
        save_file(filename || Save.save_filename, save_data) unless no_file
        # Call the hooks that restore all the data
        AFTER_SAVE_HOOKS.each_value(&:call)
        DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning save_data")
        return save_data
      end

      # Load a game
      # @param filename [String, nil] name of the save file (nil = auto name the save file)
      # @param no_load_parameter [Boolean] if the system should not call load_parameters
      # @return [PFM::Pokemon_Party, nil] The save data (nil = no save data / data corruption)
      # @note Change $pokemon_party
      def load(filename = nil, no_load_parameter: false)
        DZDebug.log(DZDebug::LogLevel::ENTRY, self, "filename = #{filename}, no_load_parameter = #{no_load_parameter}")
        filename ||= Save.save_filename
        if !File.exist?(filename)
          DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning nil")
          return nil
        end

        header = Configs.save_config.save_header
        data = File.binread(filename)
        file_header = data[0...(header.size)]
        if file_header != header
          DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning nil")
          return nil
        end

        $pokemon_party = Marshal.load(encrypt(data[header.size..-1]))
        $pokemon_party.load_parameters unless no_load_parameter
        DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning")
        return $pokemon_party
      rescue LoadError, StandardError
        DZDebug.log(DZDebug::LogLevel::EXIT, self, "Returning nil")
        return nil
      end
    end
  end
end
